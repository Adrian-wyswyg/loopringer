from dataclasses import dataclass, field
from functools import cached_property
from math import sqrt
from typing import List

import marshmallow_dataclass
import requests
from requests import Session

COINGECKO_ROOT = "https://api.coingecko.com/api/v3"
NULL_ADDRESS = "0x0000000000000000000000000000000000000000"


class Coingecko:
    def get_ethereum_price(self, currency="USD"):
        url = f"{COINGECKO_ROOT}/simple/price"
        response = requests.get(url, params=dict(ids="ethereum", vs_currencies=currency))
        response.raise_for_status()
        price_data = response.json()["ethereum"]
        return price_data[currency.lower()]

    def get_token_price(self, token, currency="USD"):

        if token.address == NULL_ADDRESS:
            return self.get_ethereum_price(currency=currency)

        url = f"{COINGECKO_ROOT}/simple/token_price/ethereum"

        response = requests.get(
            url, params=dict(contract_addresses=token.address, vs_currencies=currency)
        )
        response.raise_for_status()
        price_data = response.json()[token.address]
        return price_data[currency.lower()]


@dataclass
class OrderAmount:
    minimum: str
    maximum: str
    dust: str


@dataclass
class Token:
    type: str
    tokenId: int
    symbol: str
    name: str
    address: str
    decimals: int
    precision: int
    fastWithdrawLimit: int
    orderAmounts: OrderAmount
    enabled: bool

    def formatted(self, amount: int):
        return f"{amount / (10 ** self.decimals)} {self.symbol}"

    def price(self, gecko: Coingecko):
        return gecko.get_token_price(self)

    def __repr__(self):
        return f"<Token: {self.symbol}>"


@dataclass
class TokenPrice:
    symbol: str
    price: str
    updatedAt: int


@dataclass
class TokenVolume:
    tokenId: int
    volume: int


@dataclass
class LiquidityPoolTokenAmount:
    token: Token
    amount: float

    @property
    def formatted(self):
        return f"{self.amount} {self.token.symbol}"

    def dollar_amount(self, gecko: Coingecko):
        return self.token.price(gecko) * self.amount


@dataclass
class LiquidityPoolToken:
    pooled: List[int]
    lp: int


@dataclass
class LiquidityPoolPrecision:
    price: int
    amount: int


@dataclass
class LiquidityPool:
    name: str
    market: str
    address: str
    version: str
    tokens: LiquidityPoolToken
    feeBips: int
    precisions: LiquidityPoolPrecision
    status: int

    def token_pair(self, token_client):
        return [token_client.get_token_by_id(token_id) for token_id in self.tokens.pooled]


@dataclass
class LiquidityPoolBalance:
    FEE = 0.0025

    pooled: List[TokenVolume]
    lp: TokenVolume
    risky: bool = field(default=False)

    @property
    def ratio(self):
        one, other = self.pooled
        return one.volume / other.volume

    @property
    def k(self):
        one, other = self.pooled
        return one.volume * other.volume

    def find_pool_token_side(self, token: Token):
        one, other = self.pooled

        if one.tokenId == token.tokenId:
            return one

        elif other.tokenId == token.tokenId:
            return other

        else:
            return None

    def find_pool_token_counterside(self, token: Token):

        one, other = self.pooled

        if one.tokenId == token.tokenId:
            return other
        elif other.tokenId == token.tokenId:
            return one
        else:
            return None

    def calculate_arbitrage_swap(self, liquidity_amounts: List[LiquidityPoolTokenAmount]):
        underpriced = min(liquidity_amounts, key=lambda token: token.amount)
        overpriced = max(liquidity_amounts, key=lambda token: token.amount)

        sell_side = self.find_pool_token_side(underpriced.token)
        buy_side = self.find_pool_token_side(overpriced.token)

        spread = overpriced.amount / underpriced.amount

        arbitrage_factor = sqrt(spread) - 1

        order_size_wei = arbitrage_factor * sell_side.volume
        sell_amount = order_size_wei / (10 ** underpriced.token.decimals)

        amount_wei = buy_side.volume - (self.k / (sell_side.volume + order_size_wei))

        buy_amount = (1 - self.FEE) * (amount_wei / (10 ** overpriced.token.decimals))

        return (
            LiquidityPoolTokenAmount(token=underpriced.token, amount=sell_amount),
            LiquidityPoolTokenAmount(token=overpriced.token, amount=buy_amount),
        )

    def token_volume(self, token: Token):
        volumes = self.pooled
        volume = next((v.volume for v in volumes if v.tokenId == token.tokenId), None)
        return volume and volume / (10 ** token.decimals)


OrderAmountSchema = marshmallow_dataclass.class_schema(OrderAmount)
TokenSchema = marshmallow_dataclass.class_schema(Token)
TokenPriceSchema = marshmallow_dataclass.class_schema(TokenPrice)
LiquidityPoolSchema = marshmallow_dataclass.class_schema(LiquidityPool)
LiquidityPoolBalanceSchema = marshmallow_dataclass.class_schema(LiquidityPoolBalance)


class LoopringClient(Session):
    BASE_URL = "https://api3.loopring.io/api/v3"

    def request(self, method, url, *args, **kwargs):
        url = "/".join((self.BASE_URL, url.lstrip("/")))
        return super().request(method, url, *args, **kwargs)


class TokenClient(LoopringClient):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self._tokens = self._fetch_tokens()

    def _fetch_tokens(self):
        schema = TokenSchema(many=True)
        response = self.request("GET", "exchange/tokens")
        response.raise_for_status()
        return schema.load(response.json())

    @cached_property
    def tokens(self):
        return self._tokens

    @cached_property
    def by_symbol(self):
        return {token.symbol: token for token in self._tokens}

    @cached_property
    def by_id(self):
        return {token.tokenId: token for token in self._tokens}

    @cached_property
    def by_address(self):
        return {token.address: token for token in self._tokens}

    def get_token_by_symbol(self, symbol):
        return self.by_symbol.get(symbol)

    def get_token_by_id(self, token_id):
        return self.by_id.get(token_id)

    def get_token_by_address(self, token_address):
        return self.by_address.get(token_address)


class LiquidityPoolClient(LoopringClient):
    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        self._pools = self._fetch_pools()

    def _fetch_pools(self):
        schema = LiquidityPoolSchema(many=True)
        response = self.request("GET", "amm/pools")
        response.raise_for_status()
        return schema.load(response.json()["pools"])

    def find_by_token_pair(self, token_pair: List[Token]):
        for pool in self.pools:
            if set(pool.tokens.pooled) == set([token.tokenId for token in token_pair]):
                return pool
        return None

    @cached_property
    def pools(self):
        return self._pools

    def get_pool_balance(self, pool: LiquidityPool):
        response = self.request("GET", "amm/balance", params=dict(poolAddress=pool.address))
        response.raise_for_status()
        schema = LiquidityPoolBalanceSchema()
        return schema.load(response.json())


class Arbitrageur:
    def __init__(self):
        self._token_client = TokenClient()
        self._pool_client = LiquidityPoolClient()

    def check(self, token_symbol_pair: List[str]):
        gecko = Coingecko()

        pool = self._pool_client.find_by_token_pair(
            [self._token_client.get_token_by_symbol(s) for s in token_symbol_pair]
        )
        balance = self._pool_client.get_pool_balance(pool)
        token_pair = pool.token_pair(self._token_client)

        liquidity_amounts = [
            LiquidityPoolTokenAmount(
                token=token,
                amount=balance.token_volume(token) * token.price(gecko),
            )
            for token in token_pair
        ]

        in_amount, out_amount = balance.calculate_arbitrage_swap(liquidity_amounts)
        in_dollar = in_amount.dollar_amount(gecko)
        out_dollar = out_amount.dollar_amount(gecko)

        if out_dollar > in_dollar:
            print(f"Pay {in_amount.formatted} (${in_dollar})")
            print(f"Receive: {out_amount.formatted} (${out_dollar})")
            gross_profit = out_dollar - in_dollar
            roi = (out_dollar / in_dollar) - 1
            print(f"Profit: ${gross_profit:.3f} ({100 * roi:.3f}%)")
        else:
            print(f"{token_pair} is balanced")


def main():
    arb = Arbitrageur()

    for token in [
        "CRV",
        "DAI",
        "GRT",
        "MKR",
        "USDC",
        "LRC",
        "WBTC",
        "LINK",
        "1INCH",
        "UNI",
        "COMP",
        "PNK",
        "RENBTC",
        "AAVE",
    ]:
        arb.check(["ETH", token])
        print()


if __name__ == "__main__":
    main()
